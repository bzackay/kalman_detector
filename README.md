# kalman_detector

Implementing the Kalman filter detector for detecting
 smoothly variying signals buried in gaussian noise 
 (like fast radio bursts).

[Please do not use yet]

Main functions:
# Using the cumulative chi2 score of DFT(signal)
secondary_spectrum_cumulative_chi2_score(sig)

#Using the Kalman Score:
## First, we compute the coefficients for the exponential tail
kalman_transition_sigma_list, coeffs = kalman_prepare_coeffs(channel_stds, kalman_transition_sigma_list, n_random)

stds[i] = var[sig[i]]
sig_ts = [list of transition stds for the smooth noise process]
#coeffs = [coefficients for the tail distribution of the score. computed using previous function]
## Here we compute the score itself.
significance_kalman = kalman_significance(sig, stds, sig_ts, coeffs)


# If you use it, please cite Zackay et al (in prep)

